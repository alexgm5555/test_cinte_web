import React, { Component } from 'react'
import './App.css';
import VewList from './components/viewList/viewList';
import Header from './components/Header/Header';
import CreateList from './components/CreateList/CreateList';
import EditList from './components/EditList/EditList';
import DeleteList from './components/DeleteList/DeleteList';
import ViewTodos from './components/ViewTodos/ViewTodos';
import { BrowserRouter as Router, Route } from 'react-router-dom'



export default class App extends Component {
  constructor(props) {
    super(props);
    this.handleFindChange = this.handleFindChange.bind(this);
    this.handleFindChange2 = this.handleFindChange2.bind(this);
    this.state = { filterText: '', filterId: '' };
  }
  handleFindChange(text, type) {
    this.setState({ filterText: text });
  }
  handleFindChange2(text, type) {
    this.setState({ filterId: text });
  }
  render() {
    const routes = [
      {
        path: '/',
        exact: true,
        sidebar: () => <div>Lista Original!</div>,
        main: () => <VewList filterText={this.state.filterText} />
      },
      {
        path: '/CreateList',
        exact: true,
        sidebar: () => <div>Crear!</div>,
        main: () => <CreateList />
      },
      {
        path: '/EditList',
        exact: true,
        sidebar: () => <div>Editar!</div>,
        main: () => <EditList  />
      },
      {
        path: '/DeleteList',
        exact: true,
        sidebar: () => <div>Eliminar!</div>,
        main: () => <DeleteList  />
      },
      {
        path: '/ViewTodos',
        exact: true,
        sidebar: () => <div>Lista completa!</div>,
        main: () => <ViewTodos filterText={this.state.filterText} filterId={this.state.filterId} />
      },
      
    ]
    return (
      <div className="App">
        <Router>
        <Header onFilter={this.handleFindChange} onFilter2={this.handleFindChange2} />
        
          <div style={{ display: 'flex' }}>
            <div style={{ flex: 1, padding: '10px' }}>
              {routes.map((route) => (
                <Route
                  key={route.path}
                  path={route.path}
                  exact={route.exact}
                  component={route.main}
                />
              ))}
            </div>
          </div>
        </Router>
      </div>
    );
  }
}
