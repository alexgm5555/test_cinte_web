import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import 'bootstrap/dist/css/bootstrap.css';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
import { db } from '../Config/Config';
import ViewTodos from '../ViewTodos/ViewTodos';

export default class CreateList extends Component {
    constructor(props) {
        super(props);
        this.state = {id: 8, title: '8' }

        this.handleChangeId = this.handleChangeId.bind(this);

        this.handleChangeTitle = this.handleChangeTitle.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeId(e) {
        this.setState({
            id: e.target.value
        });
    };
    handleChangeTitle(e) {
        debugger
        this.setState({
            title: e.target.value
        });
    };
    handleSubmit(){
        db.ref('/todos').push({
            id: this.state.id,
            title: this.state.title
        });
    };
    render() {
        return (
            <div>
                <FormControl
                    type="text"
                    placeholder="Search Id"
                    className="mr-sm-2"
                    onChange={this.handleChangeId} />
                <FormControl
                    type="text"
                    placeholder="Search Title"
                    className="mr-sm-2"
                    onChange={this.handleChangeTitle} />

                <Button
                    onClick={this.handleSubmit}
                    variant="outline-success">Crear</Button>
                <ViewTodos filterText=''/>    
            </div>
        )
    }
}
