import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';


import { Link } from 'react-router-dom'

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.onChange2 = this.onChange2.bind(this);
    }
    onChange(e) {
        this.props.onFilter(e.target.value, '');
    }
    onChange2(e) {
        this.props.onFilter2(e.target.value, '');
    }
    render() {
        return (
            <div>
                <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
                    
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link><Link to="/">Lista Original</Link></Nav.Link>
                            <Nav.Link ><Link to="/CreateList">Crear</Link></Nav.Link>
                            <Nav.Link><a><Link to="/EditList">Editar</Link></a></Nav.Link>
                            <Nav.Link><a><Link to="/DeleteList">Eliminar</Link></a></Nav.Link>
                            <Nav.Link><a><Link to="/ViewTodos">Lista completa</Link></a></Nav.Link>
                        </Nav>
                    </Navbar.Collapse>

                    <Form inline>
                        <FormControl
                            type="text"
                            placeholder="Search Title"
                            className="mr-sm-2"
                            onChange={this.onChange} />
                    </Form>
                </Navbar>
            </div >
        )
    }
}