import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import 'bootstrap/dist/css/bootstrap.css';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
// import { _ } from 'lodash'
import { db } from '../Config/Config';
import ViewTodos from '../ViewTodos/ViewTodos';

const editItem = item => {
    try {
        let itemsRef = db.ref('/todos');
        itemsRef.once('value', snapshot => {
            let data = snapshot.val();
            for (var key in data) {
                if (data[key].id == item.id) {
                    db.ref('/todos/' + key).remove();
                }
            }
        });
    } catch (error) {
        alert('El Registro no existe en la base de datos', error);
    }

};
export default class EditList extends Component {
    constructor(props) {
        super(props);
        this.state = { id: 8, title: '8' }

        this.handleChangeId = this.handleChangeId.bind(this);

        this.handleChangeTitle = this.handleChangeTitle.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeId(e) {
        this.setState({
            id: e.target.value
        });
    };
    handleChangeTitle(e) {
        debugger
        this.setState({
            title: e.target.value
        });
    };
    handleSubmit() {
        editItem(this.state);
    };
    render() {
        return (
            <div>
                <FormControl
                    type="text"
                    placeholder="Search Id"
                    className="mr-sm-2"
                    onChange={this.handleChangeId} />
                <FormControl
                    type="text"
                    placeholder="Search Title"
                    className="mr-sm-2"
                    onChange={this.handleChangeTitle} />

                <Button
                    onClick={this.handleSubmit}
                    variant="outline-success">Eliminar</Button>
                     <ViewTodos filterText=''/> 
            </div>
        )
    }
}
