import React, { Component } from 'react'


export default class viewList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataFilter: [],
            currentPage: 1,
            todosPerPage: 10,
        };

    }
    async componentDidMount() {
        const originalList = await fetch('https://jsonplaceholder.typicode.com/todos')
        const data = await originalList.json()
        this.setState({ dataFilter: data})
    }

    render() {
        let textFilter = this.props.filterText.toLowerCase()
        const todos =  this.state.dataFilter.filter(function (item) {
            return item.title.toLowerCase().search(
                textFilter) !== -1;
        })
        const renderTodos = todos.map((item, index) => {
            return <span 
            key={item.id}
            id={item.id}>
                <br></br>
                Id: {item.id}
                <br></br>
                Campo: {item.title}
                <br></br>
                ------------------
            </span>   
        });

        return (
            <div>
                <div>
                    {renderTodos}
                </div>
            </div>
        )
    }
}