import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import 'bootstrap/dist/css/bootstrap.css';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
// import { _ } from 'lodash'
import { db } from '../Config/Config';
import ViewTodos from '../ViewTodos/ViewTodos';

const editItem = item => {
    try {
        let itemsRef = db.ref('/todos');
        itemsRef.once('value', snapshot => {
            let data = snapshot.val();
            for (var key in data) {
                if (data[key].id == item.id) {
                    db.ref('/todos/' + key).update({
                        id: item.id,
                        title: item.title
                    });
                }
            }
        });
    } catch (error) {
        alert('El Registro no existe en la base de datos', error);
    }

};
export default class EditList extends Component {
    constructor(props) {
        super(props);
        this.state = { id: '', title: '' }

        this.handleChangeId = this.handleChangeId.bind(this);

        this.handleChangeTitle = this.handleChangeTitle.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeId(e) {
        this.setState({
            id: e.target.value
        });
    };
    handleChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    };
    handleSubmit() {
        editItem(this.state);
        this.setState({
            title: '',
            id: ''
        });
    };
    render() {
        return (
            <div>
                <FormControl
                    value={this.state.id}
                    type="text"
                    placeholder="Search Id"
                    className="mr-sm-2"
                    onChange={this.handleChangeId} />
                <FormControl
                    value={this.state.title}
                    type="text"
                    placeholder="Search Title"
                    className="mr-sm-2"
                    onChange={this.handleChangeTitle} />

                <Button
                    onClick={this.handleSubmit}
                    variant="outline-success">Editar</Button>
                <ViewTodos filterText='' />
            </div>
        )
    }
}
