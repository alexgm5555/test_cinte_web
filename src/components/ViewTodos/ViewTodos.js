import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import 'bootstrap/dist/css/bootstrap.css';
// import { _ } from 'lodash'
import { db } from '../Config/Config';

// const editItem = item => {
//     try {
//         let itemsRef = db.ref('/todos');
//         itemsRef.on('value', snapshot => {
//             let data = snapshot.val();
//             for (var key in data) {
//                 if (data[key].id == item.id) {
//                     db.ref('/todos/' + key).remove();
//                 }
//             }
//         });
//     } catch (error) {
//         alert('El Registro no existe en la base de datos', error);
//     }

// };
export default class EditList extends Component {
    constructor(props) {
        super(props);
        this.state = { dataSource:[]}
    }
    async componentDidMount() {
        let itemsRef =  await db.ref('/todos');
        // const originalList = await fetch('https://jsonplaceholder.typicode.com/todos')
        // const data = await originalList.json()
        await itemsRef.on('value', snapshot => {
          let data = snapshot.val();
          let items = Object.values(data);
          this.setState({ isLoading: false,
            dataSource:items });
        });
      }
    render() {
        let textFilter = this.props.filterText.toLowerCase()
        // let textId = this.props.filterId
        let todos =  this.state.dataSource.filter(function (item) {
            return item.title.toLowerCase().search(
                textFilter) !== -1;
        })
        const renderTodos = todos.map((item, index) => {
            return <span 
            key={item.id}
            id={item.id}>
                <br></br>
                Id: {item.id}
                <br></br>
                Campo: {item.title}
                <br></br>
                ------------------
            </span>   
        });

        return (
            <div>
                <div>
                    {renderTodos}
                </div>
            </div>
        )
    }
}
